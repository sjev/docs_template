# {{ cookiecutter.project_name }}



{{ cookiecutter.project_short_description}}

[published docs](https://{{ cookiecutter.gitlab_username}}.gitlab.io/{{ cookiecutter.project_slug}})


## How it works

* All documentation is contained in the `docs` folder as markdown files.
* After a push to GitLab, CI automatically builds the documentation and publishes it.


## How to write docs

Choose what suits you best:

1. Checkout locally and edit in your favorite editor. Added benefit is that you can build and preview documentation with `serve_docs.sh`.
2. Edit directly on GitLab (web IDE or single file).

