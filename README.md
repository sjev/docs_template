# MKDocs cookiecutter template

This [cookiecutter](https://cookiecutter.readthedocs.io/en/latest/README.html) template will create a documentation project using `mkdocs`


## Usage

Make sure you have [`cruft`](https://github.com/cruft/cruft#installation) installed. Alternatively, you can use
 [`cookiecutter`](https://cookiecutter.readthedocs.io/en/latest/installation.html) if you are not interested in
  getting updates to the project "boilerplate" in the future.

Create a new project:

```sh
cruft create https://gitlab.com/sjev/docs_template
```

The CLI interface will ask some basic questions, such the name of the project, and then generate all the goodies
 automatically.

After that you can make it a proper git repo:

```sh
cd <your-project-slug>
git init
git add .
git commit -m "Initial commit"
```

We update this cookiecutter template regularly to keep it up-to-date with the best practices of the Python world. You
 can get the updates into your project with:

```sh
cruft update
```
